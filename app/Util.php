<?php
/**
 * Created by PhpStorm.
 * User: netaneledri
 * Date: 23/01/2018
 * Time: 22:04
 */

namespace App;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Util
{
	public static function optimizePicture($path)
	{
		$optimizer = resolve('Optimizer');
		$picture_extensions = ['jpg', 'JPG', 'jpeg', 'png', 'gif'];
		$file_extension = File::extension($path);

		if(in_array($file_extension, $picture_extensions) AND File::exists($path)){
			$optimizer->optimize($path);
		}
	}

	public static function downloadOptimizeAndStore($original_file_url, $storage_file_path)
	{
		$extension = File::extension($original_file_url);
		Storage::disk('temp')->put('temp.'.$extension, file_get_contents($original_file_url));
		self::optimizePicture(storage_path('temp/temp.'.$extension));
		$name = uniqid();
		$storage_file_path = $storage_file_path.'/'.$name.'.'.$extension;
		Storage::disk('spaces')->put($storage_file_path, File::get(storage_path('temp/temp.'.$extension)), 'public');
		Storage::disk('temp')->delete('temp.'.$extension);
		return $name.'.'.$extension;
	}
}