let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
	.extract([
		'vue',
		'moment',
		'lodash',
		'axios',
		'v-tippy',
		'vue-scrollto',
		'vue-simple-svg'
	]);

mix.scripts([
	'public/libs/vue.js',
	'public/libs/quasar/quasar.ie.pollyfills.umd.min.js',
	'public/libs/quasar/quasar.mat.umd.min.js'
], 'public/js/libs.js');

mix.styles([
	'public/libs/quasar/quasar.mat.rtl.min.css',
	'public/libs/fontawesome.css',
], 'public/css/libs.css');

mix.sass('resources/assets/sass/app.scss', 'public/css');
