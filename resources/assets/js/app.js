
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Quasar.icons.set(Quasar.icons.fontawesome)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('contact-form', require('./components/contact-form.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return {
            model: '',
	        selectedTab: 'tab-1',
	        ratingModel: 5,
	        dateModel: null,
	        monthNames: [ "ינואר","פברואר","מרץ","אפריל","מאי","יוני",
		        "יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר" ],
	        dayNames: [
		        'א',
		        'ב',
		        'ג',
		        'ד',
		        'ה',
		        'ו',
		        'ש'
	        ]
        }
    }
});
