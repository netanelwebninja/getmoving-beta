window.moment = require('moment')
window._ = require('lodash');
import Tippy from 'v-tippy'
import VueScrollTo from 'vue-scrollto'
import VueSimpleSVG from 'vue-simple-svg'

Vue.use(Tippy, {
    arrow: true
})

Vue.use(VueSimpleSVG)

Vue.use(VueScrollTo, {
	container: "body",
	duration: 1500,
	easing: "ease",
	offset: 0,
	cancelable: true,
	onDone: false,
	onCancel: false,
	x: false,
	y: true
})

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
