@extends('layouts.app')

@section('content')
    @component('components.banner')
    @endcomponent

    @component('components.home-plans')
    @endcomponent
    @component('components.home-providers')
    @endcomponent
    @component('components.home-blog')
    @endcomponent
    @component('components.home-contact')
    @endcomponent
@endsection