<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>GetMoving</title>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link rel="stylesheet" href="{{ mix('/css/libs.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}?ver={{ uniqid() }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div id="site-content">
            <div class="container">
                <header>
                    <div class="row gutter-md items-center content-center">
                        <div class="col-2 hidden">
                            <q-btn icon="menu" outline color="white" class="text-green icon-btn" rounded></q-btn>
                        </div>
                        <div class="col-xs-12 col-md-4 logo-container">
                            <div id="logo">
                                @component('components.logo', ['link' => true, 'height' => 65])
                                @endcomponent
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 hidden-xs hidden-md">
                            <div class="row gutter-md justify-end">
                                <div class="col-auto">
                                    <q-btn rounded label="דף הבית" @click="window.location.href='/'" icon="fa-home"></q-btn>
                                </div>
                                <div class="col-auto">
                                    {{--<q-btn rounded label="חברות הובלה" icon="fa-truck"></q-btn>--}}
                                    <q-btn-dropdown label="חברות הובלה" icon="fa-truck" rounded>
                                        <q-list no-border link loose>
                                            <q-item v-for="n in 7">
                                                <q-item-main>
                                                    חברות הובלה בצפון
                                                </q-item-main>
                                            </q-item>
                                        </q-list>
                                    </q-btn-dropdown>
                                </div>
                                <div class="col-auto">
                                    <q-btn rounded label="הבלוג שלנו" icon="fa-book"></q-btn>
                                </div>
                                <div class="col-auto">
                                    <q-btn rounded label="חנות חומרי אריזה" icon="fa-shopping-cart"></q-btn>
                                </div>
                                <div class="col-auto">
                                    <q-btn v-scroll-to="'#home-contact-section'"  rounded label="יצירת קשר" icon="fa-envelope"></q-btn>
                                </div>
                                <div class="col-auto">
                                    <q-btn class="text-bold" rounded color="green" label="חייגו: 073-3223322" icon="fa-phone"></q-btn>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            @yield('content')
        </div>
        @component('components.footer')
        @endcomponent
    </div>
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/libs.js"></script>
    <script src="{{ mix('/js/app.js') }}?ver={{ uniqid() }}"></script>
</body>
</html>
