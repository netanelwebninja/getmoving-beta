<div id="home-providers-section" class="home-section bg-grey-2">
    <div class="container">
        <strong class="home-section-title">
            הכירו את נבחרת המובילים של
            <span class="text-green">GetMoving</span>
        </strong>
        <div class="home-section-content">
            <q-tabs v-model="selectedTab" align="justify" no-pane-border inverted color="black">
                <q-tab slot="title" label="מובילים במרכז" name="tab-1" icon="fa-map-marker-alt"></q-tab>
                <q-tab slot="title" label="מובילים בצפון" name="tab-2" icon="fa-map-marker-alt"></q-tab>
                <q-tab slot="title" label="מובילים בשפלה" name="tab-3" icon="fa-map-marker-alt"></q-tab>
                <q-tab slot="title" label="מובילים בדרום" name="tab-4" icon="fa-map-marker-alt"></q-tab>
                <q-tab slot="title" label="מובילים באילת" name="tab-5" icon="fa-map-marker-alt"></q-tab>
                <q-tab slot="title" label="מובילים בשרון" name="tab-6" icon="fa-map-marker-alt"></q-tab>
                <q-tab-pane v-for="tab in 6" :name="'tab-' + tab">
                    <div class="row gutter-md">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" v-for="item in 8">
                            <q-card class="bg-white text-black">
                                <q-item>
                                    <q-item-side class="hidden-xs" avatar="https://getmoving.co.il/image-uploads/cl9xQ3UAdwP6YVlmJ64w.png"></q-item-side>
                                    <q-item-main>
                                        <q-item-tile label style="font-size: 18px;">סיגמה הובלות</q-item-tile>
                                        <q-item-tile sublabel>
                                        </q-item-tile>
                                    </q-item-main>
                                    <q-item-side class="text-center">
                                        <span style="display:block;float:right;margin-left: 10px;margin-top:2px;font-size: 14px;">
                                            <span style="font-size: 18px;">טוב מאוד</span><br />
                                            <span style="font-size: 12px;">20 המלצות</span>
                                        </span>
                                        <span style="margin-top:2px;display: block;float: left;font-size:35px;" class="text-bold text-green">9.5</span>
                                        {{--<strong class="text-green" style="font-size: 20px;">9.5</strong><br />--}}
                                        {{--<span class="text-bold">מצוין, 20 המלצות</span>--}}
                                        {{--<q-chip color="green" style="padding: 10px 15px;text-align: center;font-size: 18px;">--}}
                                            {{--<strong>9.5</strong><br />--}}
                                            {{--<span>מצוין</span>--}}
                                        {{--</q-chip>--}}
                                    </q-item-side>
                                </q-item>
                                <q-card-separator></q-card-separator>
                                <q-card-main>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <q-list no-border>
                                                <q-item>
                                                    <q-item-side icon="fa-check"></q-item-side>
                                                    <q-item-main>
                                                        <span>שנות ניסיון: </span>
                                                        <strong class="text-green">10</strong>
                                                    </q-item-main>
                                                </q-item>
                                                <q-item>
                                                    <q-item-side icon="fa-check"></q-item-side>
                                                    <q-item-main>
                                                        <span>שירותי אריזה: </span>
                                                        <strong class="text-green">כן</strong>
                                                    </q-item-main>
                                                </q-item>
                                            </q-list>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <q-list no-border>
                                                <q-item>
                                                    <q-item-side icon="fa-check"></q-item-side>
                                                    <q-item-main>
                                                        <span>ביטוח תכולה: </span>
                                                        <strong class="text-green">כן</strong>
                                                    </q-item-main>
                                                </q-item>

                                                <q-item>
                                                    <q-item-side icon="fa-truck"></q-item-side>
                                                    <q-item-main>
                                                        <span>מספר משאיות: </span>
                                                        <strong class="text-green">3</strong>
                                                    </q-item-main>
                                                </q-item>
                                            </q-list>
                                        </div>
                                    </div>
                                </q-card-main>
                                <q-card-separator></q-card-separator>
                                {{--<q-card-actions style="display: initial;">--}}
                                    {{--<q-btn rounded outline label="צפה בפרופיל" style="float:right;display:block;margin: 20px 10px 0px 0px;" icon="fa-hand-point-up"></q-btn>--}}
                                    {{--<q-btn rounded outline color="primary" style="float:left;display:block;margin:20px 0px 0px 10px;" label="התקשר" icon="fa-phone"></q-btn>--}}
                                    {{--<div style="clear:both;"></div>--}}
                                {{--</q-card-actions>--}}
                                <q-card-actions>
                                    <q-btn rounded outline label="צפה בפרופיל" icon="fa-hand-point-up"></q-btn>
                                    <q-btn rounded outline label="התקשר" icon="fa-phone"></q-btn>
                                </q-card-actions>
                            </q-card>
                        </div>
                    </div>
                </q-tab-pane>
            </q-tabs>
        </div>
    </div>
</div>