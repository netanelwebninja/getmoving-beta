<div id="home-blog-section" class="home-section">
    <div class="container">
        <strong class="home-section-title">
            <span>הבלוג של </span>
            <span class="text-green">GetMoving</span>
        </strong>
        <div class="home-section-content">
            <div class="row gutter-md">
                <div v-for="n in 8" class="col-xs-12 col-sm-6 col-md-3">
                    <q-card>
                        <q-card-title>
                            <span>אחסון תכולת דירה במרכז</span>
                        </q-card-title>
                        <q-card-media>
                            <img height="150" src="https://getmoving.co.il/image-uploads/epqN4UMUR20upgA2afiz.jpg" alt="">
                        </q-card-media>
                        <q-card-main>
                            <p>
                                לפניכם הפתרון המושלם לאחסון תכולה לתקופות קצרות וארוכות המותאם לכל אחד ואחת על פי דרישה מדויקת.
                            </p>
                        </q-card-main>
                        <q-card-separator></q-card-separator>
                        <q-card-actions>
                            <q-btn label="קרא עוד" rounded outline icon="fa-book"></q-btn>
                        </q-card-actions>
                    </q-card>
                </div>
            </div>
        </div>
    </div>
</div>