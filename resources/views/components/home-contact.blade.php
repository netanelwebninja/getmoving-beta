<div id="home-contact-section" class="home-section bg-grey-2">
    <div class="container">
        <strong class="home-section-title">
            <span>צוות </span>
            <span class="text-green">GetMoving</span>
            <span> כאן בשבילכם מהדירה הישנה עד לחדשה</span>
        </strong>
        <div class="home-section-content">
            <div class="row gutter-md">
                <div class="col-xs-12 col-md-8">
                    <contact-form></contact-form>
                </div>
                <div class="col-xs-12 col-md-4">
                    <q-card class="bg-white text-black" style="margin-top: -9px;">
                        <q-card-title>
                            <strong class="text-green">GetMoving</strong> כאן בשבילך
                        </q-card-title>
                        <q-card-separator></q-card-separator>
                        <q-card-main>
                            <q-list separator no-border sparse link>
                                <q-item>
                                    <q-item-side icon="fa-phone"></q-item-side>
                                    <q-item-main>
                                        <span>חייגו אלינו: </span>
                                        <strong class="text-primary">073-3223322</strong>
                                    </q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-envelope"></q-item-side>
                                    <q-item-main>
                                        <span>שלחו לנו מייל: </span>
                                        <strong class="text-primary">office@getmoving.co.il</strong>
                                    </q-item-main>
                                </q-item>
                                <q-item style="padding-bottom: 65px;">
                                    <q-item-side icon="fa-map-marker-alt"></q-item-side>
                                    <q-item-main>
                                        <strong>דוב פרידמן 2, רמת גן</strong>
                                    </q-item-main>
                                </q-item>
                            </q-list>
                        </q-card-main>
                    </q-card>
                </div>
            </div>
        </div>
    </div>
</div>