<footer>
    <div class="container">
        <div class="row gutter-md">
            <div class="col-xs-12 col-md-8">
                <div class="row">
                    <div class="col-12">
                        <strong class="q-headline text-white">
                            <q-icon name="fas fa-map"></q-icon>
                            GetMoving הובלות בפריסה ארצית
                        </strong>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 footer-links" v-for="n in 3">
                        <q-list no-border highlight>
                            <q-item v-for="n in 5">
                                <q-item-side icon="fas fa-map-marker-alt" color="white"></q-item-side>
                                <q-item-main>
                                    <a href="">הובלות בצפון</a>
                                </q-item-main>
                            </q-item>
                        </q-list>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4" id="footer-social">
                <div class="row gutter-sm">
                    <div class="col-12">
                        <strong class="q-headline text-white">
                            <q-icon name="fas fa-globe"></q-icon>
                            GetMoving ברשת
                        </strong>
                        <q-btn icon="fab fa-facebook-f" rounded class="icon-btn bg-white text-primary" v-tippy title="הפייסבוק שלנו"></q-btn>
                        <q-btn icon="fab fa-youtube" rounded class="icon-btn bg-white text-red" v-tippy title="ערוך היוטיוב שלנו"></q-btn>
                        <q-btn icon="fab fa-instagram" rounded class="icon-btn bg-white text-purple" v-tippy title="GetMoving באינסטגרם"></q-btn>
                        <q-btn icon="fab fa-google-plus-g" rounded class="icon-btn bg-white text-blue" v-tippy title="GetMoving בגוגל פלוס"></q-btn>
                    </div>
                    <div class="col-12">
                        <strong class="q-headline text-white">
                            <q-icon name="fas fa-link"></q-icon>
                            <span>קישורים נוספים</span>
                        </strong>
                        <q-list highlight no-border class="footer-links" style="padding-top:0px;">
                            <q-item>
                                <q-item-side icon="fa-arrow-left" color="white"></q-item-side>
                                <q-item-main>
                                    <a href="">התחברות מובילים</a>
                                </q-item-main>
                            </q-item>
                            <q-item>
                                <q-item-side icon="fa-arrow-left" color="white"></q-item-side>
                                <q-item-main>
                                    <a href="">התחברות לקוחות</a>
                                </q-item-main>
                            </q-item>
                        </q-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>