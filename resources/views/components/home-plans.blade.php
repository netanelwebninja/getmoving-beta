<div id="home-plans-section" class="home-section">
    <div class="container">
        <strong class="home-section-title">
            מעבר דירה לא חייב להיות כאב ראש,
            <span class="text-green">GetMoving</span> כאן בשבילך.
        </strong>
        <div class="home-section-content">
            <div class="row gutter-md">
                <div class="col-xs-12 col-md-4">
                    <q-card>
                        <q-item>
                            <q-item-side icon="fa-truck"></q-item-side>
                            <q-item-main>
                                <q-item-tile label style="font-size: 18px;">השוואת מחירי הובלה בהתאמה לתכולה שלך</q-item-tile>
                                <q-item-tile sublabel>
                                    <span>קל, מהיר ובטוח</span>
                                </q-item-tile>
                            </q-item-main>
                        </q-item>
                        <q-card-separator></q-card-separator>
                        <q-card-main>
                            <q-list no-border>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>יכולת להשוות בין כל המובילים המומלצים באזורך.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>צפייה בחוות דעת והמלצות מקוריות ומאומתות.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>מבחר עצום של הצעות מחיר ואפשרויות תשלום.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>השירות ניתן חינם ללא כל עלות וללא אותיות קטנות.</q-item-main>
                                </q-item>
                            </q-list>
                        </q-card-main>
                        <q-card-separator></q-card-separator>
                        <q-card-actions>
                            <q-btn style="width:98%;" size="lg" rounded color="green" label="התחל עכשיו" icon="fa-truck"></q-btn>
                        </q-card-actions>
                    </q-card>
                </div>
                <div class="col-xs-12 col-md-4">
                    <q-card>
                        <q-item>
                            <q-item-side icon="fa-truck"></q-item-side>
                            <q-item-main>
                                <q-item-tile label style="font-size: 18px;">השוואת מחירי הובלה בהתאמה לתכולה שלך</q-item-tile>
                                <q-item-tile sublabel>
                                    <span>קל, מהיר ובטוח</span>
                                </q-item-tile>
                            </q-item-main>
                        </q-item>
                        <q-card-separator></q-card-separator>
                        <q-card-main>
                            <q-list no-border>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>יכולת להשוות בין כל המובילים המומלצים באזורך.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>צפייה בחוות דעת והמלצות מקוריות ומאומתות.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>מבחר עצום של הצעות מחיר ואפשרויות תשלום.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>השירות ניתן חינם ללא כל עלות וללא אותיות קטנות.</q-item-main>
                                </q-item>
                            </q-list>
                        </q-card-main>
                        <q-card-separator></q-card-separator>
                        <q-card-actions>
                            <q-btn style="width:98%;" size="lg" rounded color="green" label="התחל עכשיו" icon="fa-truck"></q-btn>
                        </q-card-actions>
                    </q-card>
                </div>
                <div class="col-xs-12 col-md-4">
                    <q-card>
                        <q-item>
                            <q-item-side icon="fa-truck"></q-item-side>
                            <q-item-main>
                                <q-item-tile label style="font-size: 18px;">השוואת מחירי הובלה בהתאמה לתכולה שלך</q-item-tile>
                                <q-item-tile sublabel>
                                    <span>קל, מהיר ובטוח</span>
                                </q-item-tile>
                            </q-item-main>
                        </q-item>
                        <q-card-separator></q-card-separator>
                        <q-card-main>
                            <q-list no-border>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>יכולת להשוות בין כל המובילים המומלצים באזורך.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>צפייה בחוות דעת והמלצות מקוריות ומאומתות.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>מבחר עצום של הצעות מחיר ואפשרויות תשלום.</q-item-main>
                                </q-item>
                                <q-item>
                                    <q-item-side icon="fa-check"></q-item-side>
                                    <q-item-main>השירות ניתן חינם ללא כל עלות וללא אותיות קטנות.</q-item-main>
                                </q-item>
                            </q-list>
                        </q-card-main>
                        <q-card-separator></q-card-separator>
                        <q-card-actions>
                            <q-btn style="width:98%;" size="lg" rounded color="green" label="התחל עכשיו" icon="fa-truck"></q-btn>
                        </q-card-actions>
                    </q-card>
                </div>
            </div>
            <div class="row gutter-md"></div>
        </div>
    </div>
</div>