@extends('layouts.app')

@section('content')
    <div class="main-block">
        <h2 class="main-block-title">
            <span>מסלול הובלת פריטים בודדים</span>
        </h2>
        <div class="main-block-content">
            <q-alert icon="fa-info-circle" color="white" style="margin-bottom: 50px;">
                <span class="text-black text-bold">לקוח יקר, מסלול זה מאפשר לך לקבל הצעות מחיר היישר לנייד שלך ממובילים נבחרים להובלת הפריטים שלך.</span><br />
                <span class="text-black text-bold">מלא את הטופס שלפניך ואנחנו נדאג שמובילים נבחרים יצרו איתך קשר בהקדם.</span>
            </q-alert>

            <form action="" submit.prevent>
                <div class="row gutter-lg">
                    <div class="col-xs-12 col-md-6">
                        <q-field inset="inset" icon="fa-user" dark color="white">
                            <q-input stack-label="שם מלא" color="white" dark placeholder="הזן שם פרטי ושם משפחה"></q-input>
                        </q-field>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <q-field inset="inset" icon="fa-phone" dark color="white">
                            <q-input stack-label="מספר נייד" color="white" dark placeholder="הזן מספר נייד ליצירת קשר"></q-input>
                        </q-field>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <q-field inset="inset" icon="fa-map-marker-alt" dark color="white">
                            <q-input stack-label="מאיפה עוברים" color="white" dark placeholder="הזן את הכתובת שממנה תרצה לבצע הובלה"></q-input>
                        </q-field>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <q-field inset="inset" icon="fa-map-marker-alt" dark color="white">
                            <q-input stack-label="לאן עוברים" color="white" dark placeholder="הזן את הכתובת שאליה תרצה לבצע הובלה"></q-input>
                        </q-field>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-4">
                        <q-field inset="inset" icon="far fa-calendar-alt" dark color="white">
                            <q-datetime placeholder="בחר את תאריך ההובלה הרצוי" format="DD/MM/YYYY HH:mm" stack-label="תאריך ההובלה" color="green" dark v-model="dateModel" type="datetime" :format24h="true" :month-names="monthNames" :day-names="dayNames" ok-label="בחר" cancel-label="ביטול"></q-datetime>
                        </q-field>
                    </div>
                    <div class="col-12">
                        <q-field inset="inset" icon="fa-list" dark color="white">
                            <q-input type="textarea" stack-label="תכולה להעברה" color="white" dark placeholder="רשום כאן אילו פריטים אתה צריך להעביר"></q-input>
                        </q-field>
                    </div>
                    <div class="col-12">
                        <q-btn size="xl" icon="fab fa-telegram-plane" rounded color="green" label="שלח פנייה וקבל הצעות מחיר !"></q-btn>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @component('components.home-providers')
    @endcomponent

@endsection